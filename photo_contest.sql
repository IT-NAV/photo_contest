-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2016 at 10:17 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `photo_contest`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

CREATE TABLE IF NOT EXISTS `contest` (
  `idcontest` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `date_ending` date DEFAULT NULL,
  `details` text,
  `price` varchar(45) DEFAULT NULL,
  `photo` text NOT NULL,
  `state` int(1) DEFAULT NULL,
  PRIMARY KEY (`idcontest`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contest`
--

INSERT INTO `contest` (`idcontest`, `title`, `date_added`, `date_ending`, `details`, `price`, `photo`, `state`) VALUES
(1, 'testing for the second time', '2016-07-04', '2016-07-30', 'm sjncjl c lj balsc l c l asclosadl cblo clb lc\r\nacl acl ca ac\r\nacck kas', '$50', 'blog-image3.jpg', 1),
(2, 'Me in the Bush', '2016-07-15', '2016-07-30', 'Take a photo of you the bush and upload to win cash price of 50 000frs ($100) plus a lunch date with Roots Art, one of Cameroon''s rare jewels in the entertainment industry. He is an Award winning Movie maker and a festival manager to the renowned ArtCity Short Film Festival..', '50 000frs ($100)', '3.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contestant`
--

CREATE TABLE IF NOT EXISTS `contestant` (
  `idcontestant` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `photo` text,
  PRIMARY KEY (`idcontestant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `contestant`
--

INSERT INTO `contestant` (`idcontestant`, `email`, `phone`, `password`, `photo`) VALUES
(76, 'fuhchehenry@gmail.com', '670030450', '3a5f79c3205fca2305773af28d41e3d6da823b25', '217940_241951569250807_1512771027_n.jpg'),
(77, 'fuhchehenry@gmail.com', '670030450', '3a5f79c3205fca2305773af28d41e3d6da823b25', 'constance.jpg'),
(78, 'fuhchehenry@gmail.com', '670030450', '3a5f79c3205fca2305773af28d41e3d6da823b25', '1002678_375885285906704_1886101437708184138_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contestantjoinscontest`
--

CREATE TABLE IF NOT EXISTS `contestantjoinscontest` (
  `idcontestantJoinsContest` int(11) NOT NULL AUTO_INCREMENT,
  `contest_idcontest` int(11) NOT NULL,
  `contestant_idcontestant` int(11) NOT NULL,
  `date_joined` date DEFAULT NULL,
  PRIMARY KEY (`idcontestantJoinsContest`),
  KEY `fk_contest_has_contestant_contestant1_idx` (`contestant_idcontestant`),
  KEY `fk_contest_has_contestant_contest1_idx` (`contest_idcontest`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `contestantjoinscontest`
--

INSERT INTO `contestantjoinscontest` (`idcontestantJoinsContest`, `contest_idcontest`, `contestant_idcontestant`, `date_joined`) VALUES
(8, 1, 76, '2016-07-11'),
(9, 1, 77, '2016-07-14'),
(10, 1, 78, '2016-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `contestwinner`
--

CREATE TABLE IF NOT EXISTS `contestwinner` (
  `idcontestWinner` int(11) NOT NULL AUTO_INCREMENT,
  `contest_idcontest` int(11) NOT NULL,
  `contestant_idcontestant` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`idcontestWinner`),
  KEY `fk_contest_has_contestant_contestant2_idx` (`contestant_idcontestant`),
  KEY `fk_contest_has_contestant_contest2_idx` (`contest_idcontest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `idpost` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `state` int(1) DEFAULT NULL,
  PRIMARY KEY (`idpost`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`idpost`, `url`, `email`, `state`) VALUES
(5, '<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D1491952737767397%26id%3D1489515304677807%26substory_index%3D0&width=500" width="200" height="400" style="border:none;overflow:hidden" scrolling="true" frameborder="0" allowTransparency="false"></iframe>', 'fuhchehenry@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE IF NOT EXISTS `vote` (
  `idvotes` int(11) NOT NULL AUTO_INCREMENT,
  `stars` int(11) NOT NULL,
  `contestant_idcontestant` int(11) NOT NULL,
  PRIMARY KEY (`idvotes`),
  KEY `fk_votes_contestant_idx` (`contestant_idcontestant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contestantjoinscontest`
--
ALTER TABLE `contestantjoinscontest`
  ADD CONSTRAINT `fk_contest_has_contestant_contest1` FOREIGN KEY (`contest_idcontest`) REFERENCES `contest` (`idcontest`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contest_has_contestant_contestant1` FOREIGN KEY (`contestant_idcontestant`) REFERENCES `contestant` (`idcontestant`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contestwinner`
--
ALTER TABLE `contestwinner`
  ADD CONSTRAINT `fk_contest_has_contestant_contest2` FOREIGN KEY (`contest_idcontest`) REFERENCES `contest` (`idcontest`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contest_has_contestant_contestant2` FOREIGN KEY (`contestant_idcontestant`) REFERENCES `contestant` (`idcontestant`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `fk_votes_contestant` FOREIGN KEY (`contestant_idcontestant`) REFERENCES `contestant` (`idcontestant`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
