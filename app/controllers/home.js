/*
 * Created by Che Henry on 7/4/2016.
 */

'use strict';

app.controller('homeCtrl', function($scope,ContestIdService,Data){
    $scope.getContest = function(){
        Data.get('load_contest.php').then(function (results) {
            $scope.contests = results;
        });
    };

    $scope.readMore = function(id,title,date_ending,price,details,photo){
        ContestIdService.set([{'idcontest':id,'title':title,'deadline':date_ending,'price':price,'details':details,'photo':photo}]);
    };

    $scope.join = function(id){
        ContestIdService.set(id);
    };

    $scope.getContest();
});

