/**
 * Created by Che Henry on 7/2/2016.
 */

(function(){
    var app = angular.module('photoContest');

            app.directive('fileInput', function($parse){
                return{
                    restrict : 'A',
                    link: function(scope, elem, attrs){
                        elem.bind('change', function(){
                            $parse(attrs.fileInput).assign(scope, elem[0].files);
                            scope.$apply();
                        });
                    }
                }
            });

            //join contest controller
            app.controller('JoinContest', function($scope, $location,$rootScope, $routeParams, ContestIdService, $http,Data){
                $scope.header = "Please provide the following info!";
                $scope.contestId = ContestIdService.get(); // getting the contest id from the previous page provided by the service

                $scope.getPhoto = function(){
                    $('input[type=file]').change(function(event){
                        //$scope.picture = event.target.files[0].name;
                        $scope.picture = event.target.files[0].name;
                    });
                    return $scope.picture;
                };

                $scope.upload = function () {
                    if(angular.isObject($scope.contestId)){
                        $location.path('/');
                        Data.toast('warning',"You must select a contest!");
                    }else{

                        var fd = new FormData();
                        angular.forEach($scope.files, function(file){
                            fd.append('file',file);
                        });
                        if($scope.getPhoto()){
                            Data.post('join_contest.php',
                                {
                                    'email':$scope.email,
                                    'phone':$scope.phone,
                                    'password':$scope.pass,
                                    'id_contest':$scope.contestId,
                                    'photo':$scope.getPhoto()
                                }
                            )
                                .success(function(data,status){
                                    if(data.indexOf("successful") > -1){
                                        Data.upload('upload_photo.php',fd,angular.identity,{'Content-Type': undefined
                                        })
                                            .then(function(response) {
                                                if (data.indexOf("successful") > -1) {
                                                    Data.toast('success',data);
                                                    $location.path('/');
                                                }
                                                else{
                                                    Data.toast('error',data);
                                                    $location.path('/');
                                                }
                                            });
                                    }else{
                                        Data.toast('error',data);
                                        $location.path('/');
                                    }
                                });
                        }else{
                            Data.toast('error',"Oops! Error while retrieving photo!");
                        }


                    }
                }
            });
}());