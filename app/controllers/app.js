/**
 * Created by Che Henry on 7/6/2016.
 */

'use strict';

var app = angular.module('photoContest', ['ngRoute', 'ngAnimate', 'toaster']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                title:'Tamebec Photo Contests | Home',
                templateUrl: 'app/views/home.html',
                controller: 'homeCtrl'
            })
            .when('/join_contest', {
                title:'Tamebec Photo Contests | Join contest',
                templateUrl: 'app/views/join_contest.html',
                controller: 'JoinContest'
            })
            .when('/login', {
                title:'Tamebec Photo Contests | Admin',
                templateUrl: 'app/views/login.html'
                //controller: 'JoinContest'
            })
            .when('/winners', {
                title:'Tamebec Photo Contests | Past winners',
                templateUrl: 'app/views/winners.html',
                controller: 'pastWinners'
            })
            .when('/readmore', {
                title:'Tamebec Photo Contests | Contest',
                templateUrl: 'app/views/read_more.html',
                controller: 'readMoreCtrl'
            })
            .when('/contestants', {
                title:'Tamebec Photo Contests | Contestants',
                templateUrl: 'app/views/contestants.html'
                //controller: 'pastWinners'
            })

            .otherwise({
                redirectTo: '/'
            });
    }]);
