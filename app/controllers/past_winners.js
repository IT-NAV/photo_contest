/**
 * Created by Che Henry on 7/9/2016.
 */

'use strict';

app.controller('pastWinners', function ($scope) {
    $scope.images = [
        {'title': "Contest winner 2015",'img': "NPro_MG_7231.JPG"},
        {'title': "Contest winner 2015",'img': "NPro_MG_7232.JPG"},
        {'title': "Contest winner 2015",'img': "NPro_MG_7233.JPG"},
        {'title': "Contest winner 2015",'img': "NPro_MG_7234.JPG"},
        {'title': "Contest winner 2015",'img': "NPro_MG_7235.JPG"},
        {'title': "Contest winner 2015",'img': "NPro_MG_7236.JPG"}
    ]
});

