<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/3/2016
 * Time: 5:39 AM
 */

include('conn.php');

$data = json_decode(file_get_contents("php://input"));
$email = mysqli_real_escape_string($conn,$data->email);
$phone = mysqli_real_escape_string($conn,$data->phone);
$id = mysqli_real_escape_string($conn,$data->id_contest);
$password = sha1(mysqli_real_escape_string($conn,$data->password));
$photo = mysqli_real_escape_string($conn,$data->photo);


    //adding a contestant record
    $sql = "INSERT INTO contestant (email,phone,password,photo)
              VALUES ('$email','$phone','$password','$photo')";
    $query = mysqli_query($conn,$sql);
    $id_contestant = mysqli_insert_id($conn);
        if ($query) {
            //mapping a contest to a contestant
            $date = date('Y-m-d');
            $id_contest = (int)$id;
            $query2 = "INSERT INTO contestantjoinscontest (contest_idcontest,contestant_idcontestant,date_joined)
                    VALUES ($id_contest,$id_contestant,'$date')";
            $result = mysqli_query($conn, $query2);
            if ($result) {
                print('Join contest successful!');
            } else {
                print('Oops! Error while processing!'.mysqli_error($conn));
            }
        }
