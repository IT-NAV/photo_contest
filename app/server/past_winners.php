<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/9/2016
 * Time: 12:55 PM
 */

include('conn.php');

$sql = "SELECT title,date_added,date_ending,details,price FROM contest";

$results = mysqli_query($conn, $sql);

while($rows = mysqli_fetch_assoc($results)){
    $data[] = $rows;
}

header('Content-Type: application/json');
print json_encode($data);