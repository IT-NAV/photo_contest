<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/3/2016
 * Time: 7:11 AM
 */

include('conn.php');

$sql = "SELECT idcontest,title,date_ending,details,price,photo FROM contest WHERE state = 1";

$results = mysqli_query($conn, $sql);

while($rows = mysqli_fetch_assoc($results)){
    $data[] = $rows;
}

header('Content-Type: application/json');
print json_encode($data);