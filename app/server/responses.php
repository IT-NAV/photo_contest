<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/13/2016
 * Time: 4:52 PM
 */

function response($obj,$msg1,$msg2){
    if($obj){
        print($msg1);
    }else{
        print($msg2);
    }
}

function db_handle_select_query($conn, $query){
    $results = mysqli_query($conn, $query);

    $data = [];
    if($results){
        while($rows = mysqli_fetch_assoc($results)){
            $data[] = $rows;
        }
    }

    header('Content-Type: application/json');
    print json_encode($data);
}

function getNumContestantsThisWeek($conn){
    $date_prev = date('Y-m-d',strtotime('-7 days'));
    $date_today = date('Y-m-d');
    $sql = "SELECT COUNT(idcontestant) AS value_sum FROM contestant
      WHERE idcontestant IN (SELECT contestant_idcontestant FROM contestantjoinscontest
       WHERE date_joined >='$date_prev' AND date_joined <='$date_today')";
    $results = mysqli_query($conn, $sql);

    $data = [];
    if($results){
        while($rows = mysqli_fetch_assoc($results)){
            $data = $rows['value_sum'];
        }
    }

    header('Content-Type: application/json');
    print json_encode((int)$data);
}

function getNumActiveContests($conn){
    $sql = "SELECT COUNT(idcontest) AS value_sum FROM contest WHERE state = 1";
    $results = mysqli_query($conn, $sql);

    $data = [];
    if($results){
        while($rows = mysqli_fetch_assoc($results)){
            $data = $rows['value_sum'];
        }
    }

    header('Content-Type: application/json');
    print json_encode((int)$data);
}

function getNumAllContests($conn){
    $sql = "SELECT COUNT(idcontest) AS value_sum FROM contest";
    $results = mysqli_query($conn, $sql);

    $data = [];
    if($results){
        while($rows = mysqli_fetch_assoc($results)){
            $data = $rows['value_sum'];
        }
    }

    header('Content-Type: application/json');
    print json_encode((int)$data);
}

function getNumActivePost($conn){
    $sql = "SELECT COUNT(idpost) AS value_sum FROM post WHERE state = 1";
    $results = mysqli_query($conn, $sql);

    $data = [];
    if($results){
        while($rows = mysqli_fetch_assoc($results)){
            $data = $rows['value_sum'];
        }
    }

    header('Content-Type: application/json');
    print json_encode((int)$data);
}

function getContestantsThisWeek($conn){
    $date_prev = date('Y-m-d',strtotime('-7 days'));
    $date_today = date('Y-m-d');
    $sql = "SELECT email FROM contestant
      WHERE idcontestant IN (SELECT contestant_idcontestant FROM contestantjoinscontest WHERE date_joined >='$date_prev' AND date_joined <='$date_today' ORDER BY date_joined)";
    db_handle_select_query($conn,$sql);
}