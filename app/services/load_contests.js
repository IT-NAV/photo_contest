/**
 * Created by Che Henry on 7/8/2016.
 */

'use strict';

app.factory('LoadContestsService', function($http) {
    var contest = {};
        $http.get('app/server/php/load_contest.php')
            .success(function(images){
                contest = images;
            });
    function get() {
        return contest;
    }
    return{
        get: get
    }

});
