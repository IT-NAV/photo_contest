<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/14/2016
 * Time: 10:57 AM
 */

session_id('uid');
session_start();
session_destroy();
session_commit();