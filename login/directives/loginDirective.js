/**
 * Created by Che Henry on 7/4/2016.
 */

'use strict';

app.directive('loginDirective', function(){
    return{
        templateUrl: 'partials/tpl/login.tpl.html'
    }
});