/**
 * Created by Che Henry on 7/4/2016.
 */

'use strict';

app.factory('loginService', function($http,sessionService){
   return{
       login: function(user,scope){
           var $promise = $http.post('data/user.php', user); //send data to user.php for persistence
           $promise.then(function(msg){
               var uid = msg.data;
               if(uid){
                   sessionService.set('uid',uid);
                   window.location = "admin/";
               }
               else scope.msgtxt = "Wrong Credentials";
           })
       },

       logout: function(){
           sessionService.destroy('uid');
           window.location = "../";
       },

       islogged: function(){
           var $checkSessionServer = $http.post('../data/check_session.php');
           return $checkSessionServer;
           //if(sessionService.get('user')) return true
           //else false
       }
   }
});