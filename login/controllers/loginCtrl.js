/**
 * Created by Che Henry on 7/4/2016.
 */

'use strict';

app.controller('loginCtrl', function($scope, loginService){
    $scope.msgtxt = "Login to start your session";
    $scope.login = function(user){
        loginService.login(user,$scope); //call to login service
    };

    $scope.logout = function(){
        loginService.logout();
    }
});