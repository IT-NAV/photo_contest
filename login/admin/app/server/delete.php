<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/12/2016
 * Time: 11:37 AM
 */

include_once('../../../../app/server/conn.php');
include_once('../../../../app/server/responses.php');

$data = json_decode(file_get_contents("php://input"));
$id = mysqli_real_escape_string($conn,$data->id);
$action = mysqli_real_escape_string($conn,$data->action);

if($action == "delete_contest"){
    $photo = mysqli_real_escape_string($conn,$data->photo);

    $sql = "DELETE FROM contest WHERE idcontest = $id";
    $result = mysqli_query($conn,$sql);

    if($result){
        if(file_exists("uploads/images/".$photo)){
            unlink("uploads/images/".$photo);
        }
        print("Contest successfully deleted");
    }else{
        print("Oops! Error trying to delete contest");
    }
}

if($action == "delete_post"){

    $sql = "DELETE FROM post WHERE idpost = $id";
    $result = mysqli_query($conn,$sql);
    response($result,"Post successfully deleted","Oops! Error while deleting post");
}

