<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/3/2016
 * Time: 5:39 AM
 */

include_once('../../../../app/server/conn.php');
include_once('../../../../app/server/responses.php');

$data = json_decode(file_get_contents("php://input"));

$btn_name = mysqli_real_escape_string($conn,$data->btn_name);

if($btn_name == "Confirm Data"){
    $title = mysqli_real_escape_string($conn,$data->title);
    $price = mysqli_real_escape_string($conn,$data->price);
    $current_date = mysqli_real_escape_string($conn,$data->current_date);
    $deadline = mysqli_real_escape_string($conn,$data->deadline);
    $details = mysqli_real_escape_string($conn,$data->details);
    $photo = mysqli_real_escape_string($conn,$data->photo);

    $sql = "INSERT INTO contest (title,price,date_added,date_ending,details,photo)
              VALUES ('$title','$price','$current_date','$deadline','$details','$photo')";
    $result = mysqli_query($conn,$sql);

    response($result,"Contest successfully posted!","Oops! Error while saving contest!");
//    if($result){
//        print('Contest successfully posted!');
//    }else{
//        print('Oops! Error while saving contest!');
//    }
}

if($btn_name == "Update Data"){
    $title = mysqli_real_escape_string($conn,$data->title);
    $price = mysqli_real_escape_string($conn,$data->price);
    $current_date = mysqli_real_escape_string($conn,$data->current_date);
    $deadline = mysqli_real_escape_string($conn,$data->deadline);
    $details = mysqli_real_escape_string($conn,$data->details);
    $id = mysqli_real_escape_string($conn,$data->idcontest);

    $query = "UPDATE contest SET title = '$title',price = '$price',date_added = '$current_date',date_ending = '$deadline',details = '$details'
              WHERE idcontest = $id";

    $result2 = mysqli_query($conn,$query);

    response($result2,"Contest successfully Updated!","Oops! Error while updating contest!");
}

if($btn_name == "Update_state"){
    $id = mysqli_real_escape_string($conn,$data->idcontest);
    $state = mysqli_real_escape_string($conn,$data->state);
    if($state == 1){

        $query = "UPDATE contest SET state = 0
              WHERE idcontest = $id";

        $result2 = mysqli_query($conn,$query);

        response($result2,"Contest state successfully Updated!","Oops! Error while updating contest!");
    }else{

        $query = "UPDATE contest SET state = 1
              WHERE idcontest = $id";

        $result2 = mysqli_query($conn,$query);

        response($result2,"Contest state successfully Updated!","Oops! Error while updating contest!");
    }
}
