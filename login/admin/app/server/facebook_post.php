<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/13/2016
 * Time: 4:38 PM
 */

include_once('../../../../app/server/conn.php');
include_once('../../../../app/server/responses.php');

$data = json_decode(file_get_contents("php://input"));
$btn_name = mysqli_real_escape_string($conn,$data->btn_name);


if($btn_name == "Post now"){
    $url = mysqli_real_escape_string($conn,$data->url);
    $email = mysqli_real_escape_string($conn,$data->email);

    $sql = "INSERT INTO post (url,email) VALUES ('$url','$email')";
    $result = mysqli_query($conn,$sql);

    print(mysqli_error($conn));
    response($result,"Post successful","Oops! Error while posting");
}
if($btn_name == "Update post"){
    $url = mysqli_real_escape_string($conn,$data->url);
    $email = mysqli_real_escape_string($conn,$data->email);
    $idpost = mysqli_real_escape_string($conn,$data->idpost);

    $query = "UPDATE post SET url  = '$url',email = '$email' WHERE idpost = $idpost";
    $results = mysqli_query($conn,$query);

    response($results,"Post successfully updated","Oops! Error while updating post");
}

if($btn_name == "Update_state"){
    $id = mysqli_real_escape_string($conn,$data->idpost);
    $state = mysqli_real_escape_string($conn,$data->state);
    if($state == 1){

        $query = "UPDATE post SET state = 0
              WHERE idpost = $id";

        $result2 = mysqli_query($conn,$query);

        response($result2,"Post state successfully Updated!","Oops! Error while updating post!");
    }else{

        $query = "UPDATE post SET state = 1
              WHERE idpost = $id";

        $result2 = mysqli_query($conn,$query);

        response($result2,"Post state successfully Updated!","Oops! Error while updating post!");
    }
}

