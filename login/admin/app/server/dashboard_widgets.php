<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 7/3/2016
 * Time: 7:11 AM
 */

include_once('../../../../app/server/conn.php');
include_once('../../../../app/server/responses.php');

$data = json_decode(file_get_contents("php://input"));
$type = mysqli_real_escape_string($conn,$data->type);

if($type == 1) getNumContestantsThisWeek($conn);
if($type == 2) getNumActiveContests($conn);
if($type == 3) getNumActivePost($conn);
if($type == 4) getNumAllContests($conn);
if($type == 5) getContestantsThisWeek($conn);


