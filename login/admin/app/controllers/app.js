/**
 * Created by Che Henry on 7/6/2016.
 */

'use strict';

var app = angular.module('adminApp', ['ngRoute', 'ngAnimate', 'toaster']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                url:'/home',
                title:'Tamebec Photo Contest-Admin | Dashboard',
                templateUrl: 'app/views/dashboard.html',
                controller: 'Dashboard'
            })
            .when('/new_contest', {
                title:'Tamebec Photo Contest-Admin | New contest',
                templateUrl: 'app/views/post_contest.html',
                controller: 'PostContest'
            })
            .when('/posted_contest', {
                title:'Tamebec Photo Contest-Admin | Contest',
                templateUrl: 'app/views/posted_contest.html',
                controller: 'ContestLoader'
            })
            .when('/publish_post', {
                title:'Tamebec Photo Contest-Admin | New post',
                templateUrl: 'app/views/publish_post.html',
                controller: 'FacebookPost'
            })
            .when('/contest_entries', {
                title:'Tamebec Photo Contest-Admin | Contest entries',
                templateUrl: 'app/views/contestants.html',
                controller: 'loadImages'
            })
            .when('/all_posts', {
                title:'Tamebec Photo Contest-Admin | Posts',
                templateUrl: 'app/views/post.html',
                controller: 'PostLoader'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);

app.run(function($rootScope, $location, loginService){
    var routesPermissions = ['/'];

    $rootScope.$on('$routeChangeStart', function(){
        if(routesPermissions.indexOf($location.path()) != -1 ){

            var connected = loginService.islogged();
            connected.success(function(data){
                if(!data)
                    window.location = "../";
            });

        }
    })
});
