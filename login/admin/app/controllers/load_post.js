/**
 * Created by Che Henry on 7/13/2016.
 */

'use strict';

app.controller("PostLoader", function($scope,Data){
    $scope.btn_name = "Update Post";
    $scope.displayPosts = function(){
        Data.get('load_post.php').success(function(data){
            $scope.posts = data;
        });
    };
    $scope.displayPosts();

    $scope.getPost = function(idpost,email,url){
        $scope.id = idpost;
        $scope.email=email;
        $scope.url=url;
    };

    $scope.updatePost = function(){
        $('#myModal').modal('toggle');

        Data.post('facebook_post.php',{
            'idpost':$scope.id,
            'email':$scope.email,
            'url':$scope.url,
            'btn_name':"Update post"
        }).success(function(data){
            if(data.indexOf('successful') > -1){
                $scope.displayPosts();
                Data.toast("success", data);
            }else{
                $scope.displayPosts();
                Data.toast("Error", data);
            }
        });
    };

    $scope.deletePost = function(idpost){
        Data.post('delete.php', {'id':idpost,'action':"delete_post"}).success(function(data){
            $scope.displayPosts();
            if(data.indexOf('successful') > -1){
                Data.toast("success", data);
            }else{
                $scope.displayPosts();
                Data.toast("Error", data);
            }
        });
    };

    $scope.stateChange = function(id,state){
        Data.post('facebook_post.php',{'state':state,'idpost':id,'btn_name':'Update_state'})
            .success(function(data){
                if(data.indexOf('successful') > -1){
                    $scope.displayPosts();
                    Data.toast("success", data);
                }else{
                    $scope.displayPosts();
                    Data.toast("Error", data);
                }
            });
    }
});