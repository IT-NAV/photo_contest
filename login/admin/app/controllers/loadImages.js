/**
 * Created by Che Henry on 7/3/2016.
 */

    'use strict';
    app.controller('loadImages', function ($scope, $http, Data) {

        $scope.getImages = function(){
            Data.get('get_images.php').success(function (data) {
                $scope.images = data;
            });
        };
    });
