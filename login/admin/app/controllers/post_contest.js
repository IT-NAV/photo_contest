/**
 * Created by Che Henry on 7/2/2016.
 */

(function(){
    var app = angular.module('adminApp');

            app.directive('fileInput', function($parse){
                return{
                    restrict : 'A',
                    link: function(scope, elem, attrs){
                        elem.bind('change', function(){
                            $parse(attrs.fileInput).assign(scope, elem[0].files);
                            scope.$apply();
                        });
                    }
                }
            });

            //join contest controller
            app.controller('PostContest', function($scope, $location,$rootScope, $routeParams, $http,Data,$route){
                $scope.btn_name = "Confirm Data";
                $scope.getPhoto = function(){
                    $('input[type=file]').change(function(event){
                        //$scope.picture = event.target.files[0].name;
                        $scope.picture = event.target.files[0].name;
                    });
                    return $scope.picture;
                };

                $scope.upload = function () {
                        var fd = new FormData();
                        angular.forEach($scope.files, function(file){
                            fd.append('file',file);
                        });
                        if($scope.getPhoto()){
                            Data.post('post_contest.php',
                                {
                                    'title':$scope.title,
                                    'price':$scope.price,
                                    'current_date':$scope.current_date,
                                    'deadline':$scope.deadline,
                                    'details':$scope.details,
                                    'btn_name':"Confirm Data",
                                    'photo':$scope.getPhoto()
                                }
                            )
                                .success(function(data,status){
                                    if(data.indexOf("successful") > -1){
                                        Data.upload('upload_photo.php',fd,angular.identity,{'Content-Type': undefined
                                        })
                                            .then(function(response) {
                                                if (data.indexOf("successful") > -1) {
                                                    Data.toast('success',data);
                                                    $route.reload();
                                                }
                                                else{
                                                    Data.toast('error',data);
                                                    $route.reload();
                                                }
                                            });
                                    }else{
                                        Data.toast('error',data);
                                        $route.reload();
                                    }
                                });
                        }else{
                            Data.toast('warning',"Please select a photo!");
                        }
                }
            });
}());