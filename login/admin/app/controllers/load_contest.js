/**
 * Created by Che Henry on 7/12/2016.
 */


'use strict';

app.controller("ContestLoader", function(Data,$scope){
     $scope.displayContests = function(){
         Data.get('load_contest.php').success(function(data){
             $scope.contests = data;
         });
     };
    $scope.displayContests();

    $scope.deleteContest = function(id,photo){
        Data.post('delete.php', {'id':id,'photo':photo,'action':"delete_contest"}).success(function(data){
            if(data.indexOf('successful') > -1){
                Data.toast("success", data);
            }else{
                Data.toast("Error", "Error! Contest not deleted!");
            }
        });
        $scope.displayContests();
    };

    $scope.getContest = function(id,title,price,start_date,deadline,details){
        $scope.id = id;
        $scope.title=title;
        $scope.price=price;
        $scope.current_date=start_date;
        $scope.deadline=deadline;
        $scope.details=details;
        $scope.btn_name = "Update Data";
    };

    $scope.updateContest = function(){
        $('#myModal').modal('toggle');

        Data.post('post_contest.php',{
            'idcontest':$scope.id,
            'title':$scope.title,
            'price':$scope.price,
            'current_date':$scope.current_date,
            'deadline':$scope.deadline,
            'details':$scope.details,
            'btn_name':$scope.btn_name
        }).success(function(data){
            if(data.indexOf('successful') > -1){
                $scope.displayContests();
                Data.toast("success", data);
            }else{
                $scope.displayContests();
                Data.toast("Error", data);
            }
        });
    };

    $scope.stateChange = function(id,state){
        Data.post('post_contest.php',{'state':state,'idcontest':id,'btn_name':'Update_state'})
        .success(function(data){
                if(data.indexOf('successful') > -1){
                    $scope.displayContests();
                    Data.toast("success", data);
                }else{
                    $scope.displayContests();
                    Data.toast("Error", data);
                }
            });
    }
});