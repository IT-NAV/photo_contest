/**
 * Created by Che Henry on 7/13/2016.
 */

'use strict';

app.controller("FacebookPost",function($scope,$route,Data){
    $scope.btn_name = "Post now";
    $scope.post = function(){
        if($scope.url && $scope.email){
            Data.post('facebook_post.php',{'url':$scope.url,'email':$scope.email,'btn_name':$scope.btn_name}).success(function(data){
                if(data.indexOf('successful') > -1){
                    $route.reload();
                    Data.toast("success",data);
                }else{
                    $route.reload();
                    Data.toast("Error",data);
                }
            })
        }else{
            Data.toast("error","Oops! one or more fields are empty");
        }
    };
});