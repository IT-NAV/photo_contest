/**
 * Created by Che Henry on 7/13/2016.
 */

'use strict';

app.controller("Dashboard",function($route,Data,$scope){
    $scope.widgets = function(){
        Data.post('dashboard_widgets.php',{'type':1}).success(function(data){
            if(data > 0){
                $scope.totlContWeek = data;
            }else{$scope.totlContWeek = 0;}
        });

        Data.post('dashboard_widgets.php',{'type':2}).success(function(data){
            if(data > 0){
                $scope.activeCont = data;
            }else{$scope.activeCont = 0;}
        });

        Data.post('dashboard_widgets.php',{'type':3}).success(function(data){
            if(data > 0){
                $scope.activePost = data;
            }else{$scope.activePost = 0;}
        });

        Data.post('dashboard_widgets.php',{'type':4}).success(function(data){
            if(data > 0){
                $scope.totalAllCont = data;
            }else{ $scope.totalAllCont = 0;}
        });
    };

    $scope.contsList = function(){
        Data.post('dashboard_widgets.php',{'type':5}).success(function(data){
            if(data){
                $scope.contThisWeek = data;
            }
        })
    }
});